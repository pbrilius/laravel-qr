@extends('layouts.app')

@section('title', 'Show beneficiary')

@section('sidebar')
    @parent
    <dl>
        <dt>Beneficiary context menu</dt>
        <dd><a href="{{ url('beneficiary/' . $beneficiary->id . '/delete') }}">Delete beneficiary</a></dd>
        <dd><a href="{{ url('beneficiary/' . $beneficiary->id . '/edit') }}">Update beneficiary</a></dd>
    </dl>
@endsection

@section('content')
<table>
    <tr>
        <td>
            {{ $beneficiary->name }}
        </td>
        <td>
            {{ $beneficiary->qr }}
        </td>
    </tr>
</table>
@endsection