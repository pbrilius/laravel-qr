@extends('layouts.app')

@section('title', 'Beneficiaries list')

@section('sidebar')
    @parent
@endsection

@section('content')
    <beneficiaries-list></beneficiaries-list>
@endsection