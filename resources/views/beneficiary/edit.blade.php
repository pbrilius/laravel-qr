@extends('layouts.app')

@section('title', 'Edit beneficiary')

@section('sidebar')
    @parent
@endsection

@section('content')
<form action="/beneficiary/{{ $beneficiary->id }}/update" method="POST">
    @csrf
    <label for="name">Name</label>
    <input name="name" maxlength="255" value="{{ $beneficiary->name }}"/>
    <button type="submit">Update</button>
</form>
@endsection