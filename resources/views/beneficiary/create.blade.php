@extends('layouts.app')

@section('title', 'Add beneficiary')

@section('sidebar')
    @parent
@endsection

@section('content')
<form action="/beneficiary/store" method="POST">
    @csrf
    <label for="name">Name</label>
    <input name="name" maxlength="255"/>
    <button type="submit">Add</button>
</form>
@endsection