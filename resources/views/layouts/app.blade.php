<html>
    <head>
        <title>App Name - @yield('title')</title>
    </head>
    <body>
        @section('sidebar')
            <dl>
                <dt>Beneficiaries</dt>
                <dd><a href="{{ url('/beneficiaries') }}">Common list</a></dd>
                <dd><a href="{{ url('/beneficiary/add') }}">Add Beneficiary</a></dd>
            </dl>
        @show

        <div class="container" id="app">
            @yield('content')
        </div>
    </body>
    <script type="text/javascript" src="/js/app.js"></script>
</html>