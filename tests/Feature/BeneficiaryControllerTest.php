<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BeneficiaryControllerTest extends TestCase
{
    /**
     * Beneficiary API REST test
     *
     * @return void
     */
    public function testPostStorage()
    {
        $response = $this->postJson('/api/beneficiary', [
            'name' => 'QR on Iran',
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'name' => 'QR on Iran',
            ]);
    }

    /**
     * List beneficiaries
     *
     * @return void
     */
    public function testIndexStage()
    {
        $response = $this->getJson('/api/beneficiaries');

        $response
            ->assertOk()
            ->assertJson($response->json());
    }
}
