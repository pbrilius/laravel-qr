<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FrontBeneficiaryControllerTest extends TestCase
{
    /**
     * List case
     *
     * @return void
     */
    public function testList()
    {
        $response = $this->get('/beneficiaries');

        $response
            ->assertStatus(200)
            ->assertSee('beneficiaries-list')
            ;
    }

    public function testCreate()
    {
        $response = $this->get('/beneficiary/add');

        $response
            ->assertStatus(200)
            ->assertSee('form')
            ->assertSeeInOrder(['label', 'input'])
            ;
    }

    public function testStore()
    {
        $response = $this->post('/beneficiary/store', [
            'name' => 'QR in Iran',
        ]);

        $response
            ->assertStatus(302)
        ;
    }
}
