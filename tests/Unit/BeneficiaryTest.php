<?php

namespace Tests\Unit;

use App\Beneficiary;
use PHPUnit\Framework\TestCase;

class BeneficiaryTest extends TestCase
{
    /**
     * ORM unit case.
     *
     * @return void
     */
    public function testPrivateProperties()
    {
        $beneficiary = $this
            ->getMockBuilder(Beneficiary::class)
            ->disableOriginalConstructor()
            ->disallowMockingUnknownTypes()
            ->getMock();

        $this->assertObjectHasAttribute('table', $beneficiary);
        $this->assertObjectHasAttribute('keyType', $beneficiary);
        $this->assertObjectHasAttribute('fillable', $beneficiary);
    }
}
