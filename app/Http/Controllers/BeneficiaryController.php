<?php

namespace App\Http\Controllers;

use App\Beneficiary;
use App\Http\Resources\BeneficiaryCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BeneficiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new BeneficiaryCollection(Beneficiary::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $beneficiary = new Beneficiary();

        $beneficiary->name = $request->name;

        $response = Http::get(config('qr.base-url'), [
            'data' => $request->name,
        ]);
        $beneficiary->qr = base64_encode(utf8_encode(trim($response->body())));

        $beneficiary->save();

        return response()->json($beneficiary, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Beneficiary  $beneficiary
     * @return \Illuminate\Http\Response
     */
    public function show(Beneficiary $beneficiary)
    {
        return new BeneficiaryCollection($beneficiary);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Beneficiary  $beneficiary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Beneficiary $beneficiary)
    {
        $beneficiary->name = $request->input('name');

        return new BeneficiaryCollection($beneficiary);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Beneficiary  $beneficiary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Beneficiary $beneficiary)
    {
        $beneficiary->delete();

        return response('', 204);
    }
}
