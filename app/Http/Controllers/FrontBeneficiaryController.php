<?php

namespace App\Http\Controllers;

use App\Beneficiary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FrontBeneficiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('beneficiary.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('beneficiary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $beneficiary = new Beneficiary();
        $beneficiary->name = $request->name;

        $qr = Http::get(config('qr.base-url'), [
            'data' => $request->name,
        ]);

        $beneficiary->qr = base64_encode(utf8_encode(trim($qr->body())));

        $id = $beneficiary->id;
        $beneficiary->save();

        return redirect('beneficiary/' . $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $beneficiary = Beneficiary::find($id);

        return view('beneficiary.show', [
            'beneficiary' => $beneficiary,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $id)
    {
        return view('beneficiary.edit', [
            'beneficiary' => Beneficiary::find($id),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $id)
    {
        $beneficiary = Beneficiary::find($id);

        $beneficiary->name = $request->name;
        $qr = Http::get(config('qr.base-url', [
            'data' => $request->name,
        ]));

        $beneficiary->qr = base64_encode(utf8_encode(trim($qr)));

        $beneficiary->save();

        return redirect('beneficiary/' . $beneficiary->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $beneficiary = Beneficiary::find($id);
        
        $beneficiary->delete();

        return redirect('beneficiaries');
    }
}
