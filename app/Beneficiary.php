<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Beneficiary extends Model
{
    use SoftDeletes;
    
    protected $table = 'beneficiary';

    protected $keyType = 'string';

    protected $fillable = ['name'];

    public function __construct()
    {
        parent::__construct();

        $this->id = \uuid_create();
    }
}
