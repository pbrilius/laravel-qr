# laravel-qr

## Installation

```shell
composer install
```

## Database

```shell
cp -v .env.example .env
```

Enter the MySQL credentials and launch migrations.

## Migrations

```shell
php artisan migrate
```

## Frontend

```shell
node -v
npm -v
php artisan ui bootstrap
php artisan ui vue
npm install
npm run dev
```

## Serving

```shell
php artisan serve
```

## Navigation

* /beneficiaries
* /beneficiary/add

See others in **web.php**.

All routes listed by:

```shell
php artisan route:list
```

## TDD units

```shell
php artisan test
```

## Note

```shell
php -m | grep uuid
```

should return a positive case of UUID module installed. If not, install it with sudo access:

```shell
pecl install uuid
```