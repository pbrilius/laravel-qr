<?php

use App\Http\Controllers\FrontBeneficiaryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/beneficiaries', [FrontBeneficiaryController::class, 'index']);
Route::get('/beneficiary/add', [FrontBeneficiaryController::class, 'create']);
Route::post('/beneficiary/store', [FrontBeneficiaryController::class, 'store']);
Route::get('/beneficiary/{id}', [FrontBeneficiaryController::class, 'show']);
Route::get('/beneficiary/{id}/delete', [FrontBeneficiaryController::class, 'destroy']);
Route::get('/beneficiary/{id}/edit', [FrontBeneficiaryController::class, 'edit']);
Route::post('/beneficiary/{id}/update', [FrontBeneficiaryController::class, 'update']);
