<?php

return [
    'base-url' => env('QR_URL', 'https://qrcode.tec-it.com/API/QRCode'),
];